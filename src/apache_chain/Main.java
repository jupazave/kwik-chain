package apache_chain;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

/**
 * Created by jupazave on 10/6/17.
 */
public class Main {

    public static void main(String[] args) {
        Context context = new KwikContext();

        Command chain = new KwikGeneratorChain();

        try {
            chain.execute(context);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

