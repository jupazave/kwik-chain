package apache_chain.Commands;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

import java.util.Scanner;

/**
 * Created by jupazave on 10/6/17.
 */
public class ScannerCommand implements Command {

    private String recievePhase() {
        System.out.println("Introduce tu frase:");
        return new Scanner(System.in).nextLine();
    }

    @Override
    public boolean execute(Context context) throws Exception {
        try {
            context.put("input", recievePhase());
        } catch (Exception e){
            return true;
        }
        return false;
    }
}
