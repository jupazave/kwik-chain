package apache_chain.Commands;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

import java.util.ArrayList;

/**
 * Created by kevingamboa17 on 10/6/17.
 */
public class KwikCommand implements Command {

    public static ArrayList<String> rearange(String phrase){
        ArrayList<String> result ;

        result =  resolve(phrase);

        return result;

    }


    private static ArrayList<String> resolve(String phrase){
        ArrayList<String> result = new ArrayList<>();
        String phaseArray[] = phrase.split(" ");

        for(int i=0; i < phaseArray.length; i++) {
            String workingStr = "";
            for(int j=0; j < phaseArray.length; j++) {
                int point = j + i;
                if (point >= phaseArray.length) point = point - phaseArray.length;

                workingStr = new StringBuilder(workingStr).append(phaseArray[point]).append(" ").toString();

            }

            if(workingStr.endsWith(" ")){
                workingStr = workingStr.substring(0, workingStr.length() - 1);
            }

            result.add(workingStr);
        }


        return result;

    }

    @Override
    public boolean execute(Context context) throws Exception {
        String input = (String) context.get("input");

        context.put("results", rearange(input));

        return false;
    }
}
