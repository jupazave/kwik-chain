package apache_chain.Commands;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

import java.util.ArrayList;

/**
 * Created by jupazave on 10/6/17.
 */
public class DisplayCommand implements Command {

    public void display(ArrayList<String> result){

        result.forEach((phase)->{
            System.out.println(Uppercase(phase));
            System.out.println("===============");
        });

    }

    public String Uppercase(String workingStr) {
        return workingStr.toUpperCase();
    }


    @Override
    public boolean execute(Context context) throws Exception {



        ArrayList<String> result = (ArrayList<String> ) context.get("results");


        display(result);


        return false;
    }
}
