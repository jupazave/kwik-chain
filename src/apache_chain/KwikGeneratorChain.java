package apache_chain;

import apache_chain.Commands.DisplayCommand;
import apache_chain.Commands.KwikCommand;
import apache_chain.Commands.ScannerCommand;
import org.apache.commons.chain.impl.ChainBase;

public class KwikGeneratorChain extends ChainBase{

    KwikGeneratorChain() {
        super();
        addCommand(new ScannerCommand());
        addCommand(new KwikCommand());
        addCommand(new DisplayCommand());
    }
}
